FROM debian:bullseye

RUN apt update && \
    apt install -y --no-install-recommends ntpsec-ntpviz gnuplot-nox rsync && \
    apt clean && \
    rm -rf /var/lib/apt/lists

RUN mkdir /app /web
COPY app /app/

WORKDIR /app
CMD /app/ntpviz-loop.sh
