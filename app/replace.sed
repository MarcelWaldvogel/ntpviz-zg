#!/bin/sed -f
# Replace strings in HTML and SVG files with nicer names
s/109-202-196-249.init7.net</ntp.trifence.ch (IPv4)</
s/109.202.196.249</ntp.trifence.ch (IPv4)</

s/85-195-224-28.fiber7.init7.net</ntp.zeitgitter.net (IPv4)</
s/85.195.224.28</ntp.zeitgitter.net (IPv4)</

s/185.137.172.82.init7.net</tricorder.trifence.ch (IPv4)</
s/185.137.172.82</tricorder.trifence.ch (IPv4)</

s/dynamic-87-245-111-142.ptr.as35518.net</gpstime.trifence.ch (IPv4)</
s/87.245.111.142</gpstime.trifence.ch (IPv4)</

s/dns.3eck.net</ntp.3eck.net</
s/(dns.3eck.net)</(ntp.3eck.net)</
s/62.12.167.109</ntp.3eck.net (IPv4)</

s/192.33.96.102</time2.ethz.ch (IPv4)</

s/195.176.26.204</ntp11.metas.ch (IPv4)</
s/195.176.26.205</ntp12.metas.ch (IPv4)</
s/195.176.26.206</ntp13.metas.ch (IPv4)</

s/134.34.3.19</time2.uni-konstanz.de (IPv4)</
s/2001:7c0:2800::3:19</time2.uni-konstanz.de (IPv6)</
s/134.34.3.18</time1.uni-konstanz.de (IPv4)</
s/2001:7c0:2800::3:18</time1.uni-konstanz.de (IPv6)</

s/94.198.159.11</nts.time.nl (IPv4)</

s/82.197.188.130</eudyptula.init7.net (IPv4)</

s/ntsts.sth.ntp.se</nts.netnod.se</
s/(ntsts.sth.ntp.se)</(nts.netnod.se)</
s/sth-ts.nts.netnod.se</nts.netnod.se</
s/(sth-ts.nts.netnod.se)</(nts.netnod.se)</
s/194.58.202.202</nts.netnod.se (IPv4)</

s/192.53.103.108</ptbtime1.ptb.de (IPv4)</
s/2001:638:610:be01::108</ptbtime1.ptb.de (IPv6)</
s/192.53.103.104</ptbtime2.ptb.de (IPv4)</
s/2001:638:610:be01::104</ptbtime2.ptb.de (IPv6)</
s/192.53.103.103</ptbtime3.ptb.de (IPv4)</
s/2001:638:610:be01::103</ptbtime3.ptb.de (IPv6)</

s/162.159.200.1</time.cloudflare.com (162.159.200.1)</
s/162.159.200.123</time.cloudflare.com (162.159.200.123)</

s/200.160.7.186</a.st1.ntp.br (IPv4)</
s/2001:12ff:0:7::186</a.st1.ntp.br (IPv6)</
s/201.49.148.135</b.st1.ntp.br (IPv4)</
s/200.186.125.195</c.st1.ntp.br (IPv4)</
s/200.20.186.76</d.st1.ntp.br (IPv4)</
s/200.160.7.197</gps.ntp.br (IPv4)</
s/2001:12ff:0:7::197</gps.ntp.br (IPv6)</

# Allow to open the SVG
s/\(<img src=["']\([^"']*\)["'][^>]*>\)/<a href="\2" target="_blank">\1<\/a>/
