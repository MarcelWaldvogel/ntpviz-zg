#!/bin/sh
INTERVAL=61

viz() {
  ntpviz -w s --terminal=svg "$@"
}
exclude="--exclude footer --exclude header --exclude optionfile --exclude prefix"

find . -name "*.IN" -print0 | xargs -0 ./substitute.sh

rsync -au $exclude web/. /web
while true
do
  viz --name="${THISHOST} weekly" --outdir=week --period=7
  ./clean.sh week
  rsync -au $exclude week /web
  for i in 1 2 3 4 5 6
  do
    viz --name="${THISHOST} daily" --outdir=day --period=1
    ./clean.sh day
    rsync -au $exclude day /web
    sleep $INTERVAL
  done
done
