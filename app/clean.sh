#!/bin/sh
cd $1
p1=`mktemp`
sed -n -e '/^<h1/,/-- HEADER --/p' < index.html > $p1
p2=`mktemp`
sed -n -e '/-- HEADER --/,$p' < index.html | ../replace.sed > $p2
cat prefix $p1 $p2 > nice.html
mv nice.html index.html
rm $p1 $p2

for i in *.svg
do
  ../replace.sed < $i > $i+ && mv $i+ $i
done
